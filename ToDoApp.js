import React ,{
    Component
} from 'react';
import InputBox from './InputBox';
import ToDoList from './ToDoList';
var previous;
const css = require("./style.css");
class ToDoApp extends Component {
    constructor() {
        super();
        this.updateList = this.updateList.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.reorder = this.reorder.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
        this.id_gen = 0;
        this.state = {
            todos: []
        }
    }
    updateList(data){
        let id = this.id_gen + 1;
        this.id_gen=id;
        this.setState({todos:this.state.todos.concat([{"id":id,"taskname":data,"status":false}])})
    }
    reorder(count, current)
    {
        
        if(count == 0)
        {
            previous = current;
            console.log("drag started")
        }
        else{
            let previous_index;
            let current_index;
            let index = 0;
            let deepcopy = this.state.todos.map(function(data) {
               if(data.id == previous)
               {
                previous_index = index;

               }
               else if(data.id == current){
                    current_index = index;
               }
               index++;
               return data;
           })
           let temp = deepcopy[previous_index];
           deepcopy.splice(previous_index,1);
           deepcopy.splice(current_index,0,temp);
           this.setState({todos:deepcopy});
            
        }
    }
    deleteItem(id)
    {
        let index=0;
        let index1;
        let data_copy =this.state.todos.map(function(data){
            if(data.id == id){
                index1 = index; 
            }
            index++;
            return data;})
            data_copy.splice(index1,1);
            this.setState({todos:data_copy})
    }
    changeStatus(id){
        console.log("id:"+id)
      let list = this.state.todos.map(todo =>{
               
        if(todo.id == id && todo.status == false){
                    todo.status = true;
                }
                else if(todo.id == id && todo.status == true){
                    todo.status = false;
                }
                return todo;
        }
    )
    this.setState({todos:list})
    }
    render()
    {
        return (
            <div>
                <span>ToDoList</span>
            <InputBox todos={this.updateList}/>
            <ToDoList items={this.state.todos} deleteItem = {this.deleteItem} 
            reorder={this.reorder} strike={this.changeStatus}/>
            </div>
        )
    }
}

export default ToDoApp;
