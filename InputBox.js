import React ,{Component} from 'react';


const InputBox = (props) => {
    function handleClick(e){
        if(e.key == 'Enter'){
        props.todos(e.target.value);
        e.target.value = '';
        }
    }
    return(<div className = "textbox">
        <input  type="text" onKeyPress={handleClick}/></div>
    )

}

export default InputBox