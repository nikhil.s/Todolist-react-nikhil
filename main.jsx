import React from 'react';
import ReactDom from 'react-dom';
import ToDoApp from './ToDoApp';

var destination = document.getElementById("app");
ReactDom.render(
    <ToDoApp />,
     destination
)