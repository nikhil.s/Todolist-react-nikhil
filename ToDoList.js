import React, {
    Component
} from 'react';
import ToDoItem from './ToDoItem';

const ToDoList = (props) => {
        return ( <div >
                 {
                    props.items.map(element => < ToDoItem todos = {element}
                        deleteItem = {props.deleteItem} reorder={props.reorder} 
                        strike={props.strike}/>) 
                    }
                      </div >
                    )
                }

                export default ToDoList