import React, {Componenet} from 'react';

const ToDoItem = (props) => {
    function deleteItem(){
        props.deleteItem(props.todos.id);
    }
    function dragstart(){
        props.reorder(0,props.todos.id);
       
    }
    function allowDrop(e){
        e.preventDefault();
    }
    function drop()
    {
        props.reorder(1,props.todos.id)
    }
    function strike()
    {
        props.strike(props.todos.id);
    }
    if(props.todos.status == true){
    return(
 <div className="itemlist">  <li draggable="true" onDragOver={allowDrop} 
        onDragStart={dragstart} onDrop={drop}>
        <span style={{textDecoration:"line-through"}} onClick={strike}>{props.todos.taskname}</span>
<button onClick = {deleteItem}>delete</button>
 </li>
 </div>
)
}
else if(props.todos.status == false){
  return (  <div className="itemlist">
  <li draggable="true" onDragOver={allowDrop} 
    onDragStart={dragstart} onDrop={drop}>
    <span  onClick={strike}>{props.todos.taskname}</span>
<button onClick = {deleteItem}>delete</button>
</li>
</div>
  )
} 
}
export default ToDoItem;